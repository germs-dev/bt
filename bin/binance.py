import os
import csv
from datetime import datetime
from binance.client import Client
from binance.exceptions import BinanceAPIException, BinanceOrderException

# Get API key from environment variable
api_key = os.getenv('BINANCE_API_KEY')
api_secret = os.getenv('BINANCE_API_SECRET')

try:

    # Initialize the client
    client = Client(api_key, api_secret)

    # Get account info
    info = client.get_account()

    # Calculate number of non-zero balances
    non_zero_balances = 0
    for balance in info['balances']:
        if float(balance['free']) > 0:
            non_zero_balances += 1

    print("Non-zero balances: ", non_zero_balances)

    # Get all open orders
    open_orders = client.get_open_orders()
    print("Open orders: ", len(open_orders))

    # Open file using CSV library
    with open('summary.csv', 'r') as file:
        reader = csv.reader(file)
        next(reader)
        for row in reader:
            # Get date from column 0
            date = row[0]
            dt = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')

            # Get symbol from column 1
            exchange_and_symbol = row[1]

            # Split out the symbol into two coins
            tokens = exchange_and_symbol.split('-')

            # Join 1 and 2 into a symbol like ETHBTC
            symbol = tokens[1] + tokens[2]

            print(f"{dt} {symbol}")

            symbol_spot = client.get_symbol_ticker(symbol=symbol)
            price = float(symbol_spot['price'])
            print(price)

    # try to execute
    symbol_spot = client.get_symbol_ticker(symbol="ETHBTC")
    eth_price = float(symbol_spot['price'])
    print("ETHBTC: ", eth_price)

except BinanceAPIException as e:
    print("API error: ", e)

except BinanceOrderException as e:
    print("Order error: ", e)
