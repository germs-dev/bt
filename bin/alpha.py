import requests
import os

# get api key from environment variable
api_key = os.environ.get('ALPHAVANTAGE_API_KEY')
token = "IBM"

# replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
url = f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={token}&apikey={api_key}"
r = requests.get(url)
data = r.json()

# Create directory if it doesn't exist
path = "/tmp/tokens"
if not os.path.exists(path):
    os.makedirs(path)

# Construct file name base on symbol
file = f"{path}/Alpha-{token}.json"

print("Writing to file: ", file)

# Create file and write data to it
with open(file, 'w') as f:
    f.write(str(data))
