#!bin/bash

mkdir -p /tmp/tokens

# Calculate number of entries
readonly num_lines=$(wc -l < pairs.txt)
current_line=0

# Fetch JSON for each currency pair
cat pairs.txt | while read line; do

    # Tokenise the line
    if [[ $line =~ ([0-9A-Z]+)[[:space:]]+([0-9A-Z]+)[[:space:]]+([A-Za-z]+) ]]; then

        from=${BASH_REMATCH[1]}
        to=${BASH_REMATCH[2]}
        exchange=${BASH_REMATCH[3]}
        period=hour

        # Dump the JSON to tmp
        json_filename=/tmp/tokens/$exchange-$from-$to-$period.json
        echo "[$current_line/$num_lines] $from-$to @ $exchange written to $json_filename"

        # Get prices
        curl --silent "https://min-api.cryptocompare.com/data/histo$period?fsym=$from&tsym=$to&e=$exchange&api_key=$CCAPI&limit=2000" > $json_filename    
    	sleep 0.5

        # Increment counter
        current_line=$((current_line + 1))
    fi
done

echo
ls -l /tmp/tokens/*.json
