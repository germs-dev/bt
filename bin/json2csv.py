#!/bin/python3

import sys
import json

# Read JSON from stdin
data = json.load(sys.stdin)

# Print header
print("time,open,high,low,close,volume")

# Print each data row
for row in data["Data"]:
    print(row["time"], end=",")
    print(row["open"], end=",")
    print(row["high"], end=",")
    print(row["low"], end=",")
    print(row["close"], end=",")
    print(row["volumeto"] + row["volumefrom"])
