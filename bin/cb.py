from coinbase.wallet.client import Client
import os

# Set your API key and secret key here.
api_key = os.getenv('COINBASE_API_KEY')
api_secret = os.getenv('COINBASE_API_SECRET')

try:
    print("get accounts")
    client = Client(api_key, api_secret)
    accounts = client.get_accounts()

    for account in accounts['data']:
        print("Fetching orders for account:", account['name'])
        transactions = client.get_transactions(account['id'])
        for transaction in transactions['data']:
            print("Order ID: ", transaction['id'],
                  "Amount: ", transaction['amount']['amount'],
                  "Currency: ", transaction['amount']['currency'])

except Exception as e:
    print(e)

