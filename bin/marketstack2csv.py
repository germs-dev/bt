#!/bin/python3

import sys
import json
from dateutil import parser

# Read JSON from stdin
data = json.load(sys.stdin)

# Print header
print("time,open,high,low,close,volume")

# Print each data row
if "data" in data:
  
  # Marketstack returns data in reverse chronological order
  data["data"].reverse()

  # Print each row
  for row in data["data"]:
      
      epoch = parser.parse(row["date"]).timestamp()

      print(epoch, end=",")
      print(float(row["open"]), end=",")
      print(float(row["high"]), end=",")
      print(float(row["low"]), end=",")

      # if close is a nonetype set to zero
      if row["close"] is None:
          row["close"] = 0.0

      print(float(row["close"]), end=",")

      # if volume is a nonetype set to zero
      if row["volume"] is None:
          row["volume"] = 0.0

      print(float(row["volume"]))
