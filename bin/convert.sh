#!bin/bash

# For each JSON file, convert to CSV in parallel

# Stocks
find /tmp/stocks -name '*.json' | parallel --eta -j $(nproc) 'cat {} | python3 bin/marketstack2csv.py > {.}.csv'
ls -l /tmp/stocks/*.csv

# Crypto
find /tmp/tokens -name '*.json' | parallel --eta -j $(nproc) 'cat {} | python3 bin/json2csv.py > {.}.csv'
ls -l /tmp/tokens/*.csv
