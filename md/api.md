# APIs for algorithmic trading

If you're in the UK, not Binance! See [this](https://www.investopedia.com/binance-uk-7255139) Investopedia article for more information. EDIT: this may have been addressed.

So, Coinbase it is. You do get quite overwhelmed with MFA when you're setting up the API key, and weirdly it doesn't seem to work properly in Firefox (it doesn't show you the secret key). But it does work in Chrome, so that's fine.

## Exchange opening times

Whilst crypto never sleeps, traditional exchanges have some seemingly restrictive opening times. For example, the London Stock Exchange is open from 8am to 4:30pm, Monday to Friday; and official NASDAQ Stock Market Trading Hours run from 9:30 am to 4:00 pm EST (14:30 to 21:00 BST), Monday through Friday, with the exception of market holidays.
