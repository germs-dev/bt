# Developing using Copilot

So this is just some thoughts on my first non-trivial project using Copilot.

## Adoption of AI tools

At the time of writing most companies are resistant to using Copilot.

## Suspicious minds

Most senior developers think their code is unique; actually most things have been done before. If you're doing something that is genuinely new, then are you changing the world or writing unmaintainable code?

A lot of it is just being experienced and knowing the computer science terminology. Then you can quickly recognise if a solution is good enough, or even write something that works in an unfamiliar language.

## Code correctness

There are concerns about correctness of code; but if it passes the tests/benchmarks and is in a similar style to the rest of the codebase then what is correct? All significant codebases have dark corners that are not well understood, with terrible code that goes against the coding standard and is untested but works. Well, it seems to work. Until you find it works but for the wrong reasons.

## Things I didn't expect

If you know how to code already then it's basically a very clever code completion tool. 

It also suggests things that I hadn't considered but are actually quite neat solutions. And language features that I didn't know existed.

## Lines of code

After a month or so I ran the standard `sloccount` tool on the project. I have always quite liked it as a rough finger in the air, but perhaps we need new ways to measure these things?

```bash
Total Physical Source Lines of Code (SLOC)                = 3,558
Development Effort Estimate, Person-Years (Person-Months) = 0.76 (9.10)
 (Basic COCOMO model, Person-Months = 2.4 * (KSLOC**1.05))
Schedule Estimate, Years (Months)                         = 0.48 (5.79)
 (Basic COCOMO model, Months = 2.5 * (person-months**0.38))
Estimated Average Number of Developers (Effort/Schedule)  = 1.57
Total Estimated Cost to Develop                           = $ 102,425
 (average salary = $56,286/year, overhead = 2.40).
SLOCCount, Copyright (C) 2001-2004 David A. Wheeler
SLOCCount is Open Source Software/Free Software, licensed under the GNU GPL.
SLOCCount comes with ABSOLUTELY NO WARRANTY, and you are welcome to
redistribute it under certain conditions as specified by the GNU GPL license;
see the documentation for details.
Please credit this data as "generated using David A. Wheeler's 'SLOCCount'."
```