# Backtesting

[![pipeline](https://gitlab.com/germs-dev/bt/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/bt/-/pipelines/)

## Motivation

I've had a few attempts at writing a backtester but previously I have tried to exercise hundreds of permutations of parameters with the hope that the best rises to the top; however, thousands of trades and variables are very difficult to reason about. So, I now have a single strategy with a small set of parameters which runs across a few hundred currency pairs.

## Parameters

There is a single take profit and stop loss, and a fixed trading window size.

## The trading strategy

By analysing recent changes in the value of an asset, we aim to build confidence in placing an order at the _current_ price.

1. The normalised ATR is greater than a threshold
2. There has been a recent minimum (a dip)
3. The latest price is below VWAP
4. The VWAPs are intersecting
5. Latest volume is greater than the average volume
6. The opening price is above a threshold

## Closing trades

A position is closed when the take profit is acheived, the stop loss is reached, or the trade has simply timed out (see the default trading window.) An interesting detail that I didn't appreciate initially is that you get the stop loss that _caused_ your trade to close, which could be very different to a fixed percentage.

## Opening trades

Crypto obviously never sleeps so it didn't seem to matter when I ran my nightly. However, NASDAQ opens at 2pm UK time, so that should be the perfect time, surely? Maybe not! I'm using hourly data so running at 2pm actually gets the previous night's final price, and the backtester can trigger on this. But by the time the nightly wakes up that moment has already passed. I could obviously run it hourly but that would hammer my API allowance and fetching _everything_ every hour is pretty inefficient. If I do run regularly I should be just fetching the most recent prices and storing the previous somewhere.

## Fees

Remember there is spread to be taken into account with all trades. There may well be a withdrawal fee too, eToro has a £5 fee. It does offer "after hours" trading but a sell order will just be queued until the market opens: execute "At Market" and you'll get the opening price.
