# Crypto/stock backtester [![pipeline](https://gitlab.com/germs-dev/bt/badges/main/pipeline.svg)](https://gitlab.com/germs-dev/bt/-/pipelines/)

See my other projects: [turpin.dev](https://turpin.dev).

## Disclaimer

Don't invest what you can't afford to lose. Illustrations are for illustrative purposes only. History is not an indicator of future performance. That said...

## Overview

Historic price data are fetched daily via the [Marketstack](https://marketstack.com/) and [CryptoCompare](https://min-api.cryptocompare.com/) APIs, and converted to CSV format using Python; each currency pair is then analysed in C++. Finally, the table below is generated using the trade summary.

## Profiling

The C++ code is profiled using `gprof` and visualised with `gprof2dot` and `graphviz`.

[![`gprof` profile](prof.svg)](prof.svg)
