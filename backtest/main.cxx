#include "backtest.h"
#include "fx/constants.h"
#include "summary.h"
#include <benchmark/benchmark.h>
#include <filesystem>
#include <fstream>
#include <ranges>
#include <string_view>
#include <vector>
#include <print>

/// Convert arguments to files, validate them as CSVs and backtest
int main(int argc, char **argv) {

  using namespace std::literals::string_view_literals;

  // Create vector of only CSV files
  std::vector<std::string_view> files;
  for (int i = 0; i < argc; ++i)
    if (std::filesystem::path(argv[i]).extension() == ".csv"sv)
      files.push_back(argv[i]);

  // Run backtests
  const auto summary = to_summary(files);

  // Create results
  const auto markdown = to_markdown(summary);
  const auto csv = to_csv(summary);

  // Report results
  std::println(
      "- {} currency pairs (from Marketstack, Coinbase, Kraken, Binance)", std::size(files));
  std::println("- {}% take profit", fx::take_profit);
  std::println("- {}% stop loss", fx::stop_loss);
  std::println("- {} candles window size", fx::win);
  std::println("- {} minimum ATR (normalised)", fx::minimum_atr);
  std::println("- {} minimum entry price\n", fx::minimum_entry);
  std::println("{}", markdown);

  // Write CSV to file
  std::ofstream{"summary.csv"} << csv;

  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
}
