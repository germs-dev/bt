#include "backtest.h"
#include "fx/core.h"
#include "fx/trade.h"
#include "utils.h"
#include <mutex>
#include <string>

/// Take a vector of files to backtest with testing params, and return a summary
/// of trades across all files
std::vector<trade_t> to_summary(std::span<const std::string_view> files) {

  // Results container and access mutex
  std::vector<trade_t> results;
  auto &&results_mutex = std::mutex{};

  // For each file
  std::ranges::for_each(files, [&](auto &&csv) {
    using namespace std::views;

    // Get price data from disk
    auto &&contents = file_read(csv);
    auto &&series = to_series3(contents);

    // Calculate all trade windows, remove non trades and
    // fold each window into a single price
    auto &&entries = series | slide(fx::win) | filter(fx::is_entry2) |
                     transform(fx::to_last);

    // Evaluate each trade
    for (auto &&x : entries) {

      // Find the entry iterator
      auto &&entry_it = std::ranges::find_if(
          series, [&](auto y) { return fx::to_time(x) == fx::to_time(y); });

      // Calculate index into series
      auto &&offset = std::ranges::distance(std::cbegin(series), entry_it);

      // Calculate trade window size but don't run off the end
      auto trade_window_size =
          std::min(std::size(series) - offset, static_cast<size_t>(fx::win));

      // Create trade window by dropping everything before the entry
      // iterator
      auto &&trade = std::vector<std::vector<double>>{
          std::cbegin(series) + offset,
          std::cbegin(series) + offset + trade_window_size};

      // Calculate profit
      auto &&entry = fx::to_first(trade);
      auto &&exit = fx::to_exit(trade);
      auto &&profit = fx::to_profit(fx::to_spot(entry), fx::to_spot(exit));

      // Calculate duration of trade
      auto &&entry_time = fx::to_time(entry);
      auto &&exit_time = fx::to_time(exit);
      auto &&duration = exit_time - entry_time;

      // Trim file name for reporting
      auto &&to_token = csv | take(std::ranges::size(csv) - 4uz) | drop(12uz);

      // Store results
      std::scoped_lock lock(results_mutex);
      results.push_back({to_token, entry_time, fx::to_spot(entry),
                         fx::to_spot(exit), profit, duration});
    }
  });

  // Sort results
  std::ranges::sort(
      results, [](auto a, auto b) { return std::get<1>(a) > std::get<1>(b); });

  return results;
}
