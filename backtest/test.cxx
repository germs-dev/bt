#include "test.h"
#include "utils.h"
#include <benchmark/benchmark.h>

void BM_to_series(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_series(csv1));
}

void BM_to_series3(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_series3(csv1));
}

void BM_to_series4(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_series4(csv1));
}

BENCHMARK(BM_to_series);
BENCHMARK(BM_to_series3);
BENCHMARK(BM_to_series4);
