#pragma once

#include "backtest.h"
#include <span>
#include <string>

std::string to_csv(std::span<const trade_t>);
std::string to_markdown(std::span<const trade_t>);
