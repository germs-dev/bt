#pragma once

#include "fx/constants.h"
#include <string>
#include <string_view>
#include <vector>

std::string epoch_to_utc(const size_t);
std::string file_read(const std::string_view);
std::vector<std::vector<double>> to_series(const std::string);
std::vector<std::vector<double>> to_series3(const std::string &);
std::vector<std::vector<double>> to_series4(const std::string &);
