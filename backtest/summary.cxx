#include "summary.h"
#include "utils.h"
#include <algorithm>
#include <cassert>
#include <format>

/// Convert summary of trades to CSV string
std::string to_csv(std::span<const trade_t> summary) {

  std::string out{};

  // Calculate average profit over all trades
  const auto average_profit =
      std::empty(summary)
          ? 0.0
          : std::ranges::fold_left(summary, 0.0,
                                   [](const auto acc, const auto trade) {
                                     return acc + std::get<4>(trade);
                                   }) /
                std::size(summary);

  // Header row
  out += std::format(
      "Time,Token,Open,Close,Hours,Profit ({:.2f}% avg, {} trades)\n",
      average_profit, std::size(summary));

  // Output each trade as a row
  for (const auto &s : summary) {
    auto [file, entry_time, entry, exit, profit, duration] = s;
    const auto utc = epoch_to_utc(entry_time);
    out += std::format("{},{},{:.2f},{:.2f},{},{:.1f}\n", utc, file, entry,
                       exit, duration / 3600, profit);
  }

  return out;
}

/// Convert summary of trades to markdown string
std::string to_markdown(std::span<const trade_t> summary) {

  auto out = std::string{};

  // Print summary of all trades
  if (not std::empty(summary)) {

    // Calculate average profit
    const auto average_profit =
        std::empty(summary)
            ? 0.0
            : std::ranges::fold_left(summary, 0.0,
                                     [](const auto acc, auto &&trade) {
                                       auto [file, entry_time, entry, exit,
                                             profit, duration] = trade;

                                       return acc + profit;
                                     }) /
                  std::size(summary);

    out += std::format("{:.2f}% average profit over {} trades.\n\n",
                       average_profit, std::size(summary));

    // Table heading
    out += std::format("|Time|Token|Open|Close|Hours|Profit|\n");
    out += std::format("|---|---|---|---|---|---|\n");

    // Table contents
    for (const auto &trade : summary) {
      auto [file, entry_time, entry, exit, profit, duration] = trade;
      const auto utc = epoch_to_utc(entry_time);
      out += std::format("|{}|{}|{:.2f}|{:.2f}|{}|{:.1f}%|\n", utc, file, entry,
                         exit, duration / 3600, profit);
    }
  }

  return out;
}
