#include "utils.h"
#include <algorithm>
#include <benchmark/benchmark.h>
#include <cassert>
#include <charconv>
#include <fstream>
#include <ranges>
#include <sstream>
#include <vector>

/// Open a file a return a string of the contents
std::string file_read(const std::string_view file_name) {

  /// Open file and return the contents as a string
  std::ifstream in{std::string{file_name}};

  if (not in.good())
    return {};

  // Run to the end
  in.seekg(0, std::ios::end);

  // Reserve enough storage for the file contents
  std::string out(in.tellg(), '\0');

  // Back to the start
  in.seekg(0, std::ios::beg);

  // Copy the file contents
  in.read(out.data(), out.size());

  return out;
}

/// Take a row of data, split on delimiter and return a row of floating points
const auto split_on_comma = [](auto &row) {
  using namespace std::views;
  return row | split(',') | transform([](auto &&val) -> double {
           auto str = std::string{cbegin(val), cend(val)};
           double value{};
           std::from_chars(str.data(), str.data() + str.size(), value);
           return value;
         });
};

void BM_split_on_comma(benchmark::State &state) {
  auto &&row = std::string{"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0"};
  for (auto _ : state) {
    auto &&xs = split_on_comma(row);
    benchmark::DoNotOptimize(xs);
  }
}

BENCHMARK(BM_split_on_comma);

/// Take a row of data, split on delimiter and return a row of floating points
const auto split_on_comma2 = [](auto &&row) {
  using namespace std::views;
  return row | split(',') | transform([](auto &&val) {
           auto &&str = std::string{cbegin(val), cend(val)};
           auto value = double{};
           std::from_chars(str.data(), str.data() + str.size(), value);
           return value;
         });
};

void BM_split_on_comma2(benchmark::State &state) {
  auto &&row = std::string{"1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0"};
  for (auto _ : state) {
    auto &&xs = split_on_comma2(row);
    benchmark::DoNotOptimize(xs);
  }
}

BENCHMARK(BM_split_on_comma2);

/// Open a CSV file and return a vector of floating points for each row
std::vector<std::vector<double>> to_series(const std::string csv) {

  // Read line by line
  std::istringstream in{csv};
  std::ranges::istream_view<std::string> rows(in);

  // Read prices, drop the heading, and split each row
  return rows | std::views::drop(1) | std::views::transform(split_on_comma)
    | std::ranges::to<std::vector<std::vector<double>>>();
};

/// Open a CSV file and return a vector of floating points for each row
std::vector<std::vector<double>> to_series3(const std::string &csv) {

  // Read line by line
  std::istringstream in{csv};
  std::ranges::istream_view<std::string> rows(in);

  // Read prices, drop the heading, and split each row
  auto &&px =
      rows | std::views::drop(1) | std::views::transform(split_on_comma2);

  // Create a default row to populate
  auto &&xs = std::vector<double>(fx::cells);

  // Evaluate the view by copying into a vector
  auto &&series = std::vector<std::vector<double>>{};

  for (auto v : px) {

    // Copy the individual prices
    std::ranges::copy(v, std::begin(xs));

    // And store the full complement
    series.emplace_back(xs);
  }

  return std::move(series);
};

/// Open a CSV file and return a vector of floating points for each row
std::vector<std::vector<double>> to_series4(const std::string &csv) {

  // Read line by line
  std::istringstream in{csv};
  std::ranges::istream_view<std::string> rows(in);

  // Read prices, drop the heading, and split each row
  auto &&px =
      rows | std::views::drop(1) | std::views::transform(split_on_comma2);

  // Evaluate the view by copying into a vector
  auto &&series = std::vector<std::vector<double>>{};
  series.reserve(std::ranges::distance(px));

  std::ranges::transform(px, std::back_inserter(series), [&](auto &&v) {
    // Copy the individual prices
    auto &&xs = std::vector<double>(fx::cells);
    std::ranges::copy(v, std::begin(xs));
    return xs;
  });

  return series;
};
