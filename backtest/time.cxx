#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>

/// Routine to convert epoch seconds to a UTC time string
std::string epoch_to_utc(const size_t epoch) {
  std::time_t t = epoch;
  std::tm tm = *std::gmtime(&t);
  std::ostringstream oss;
  oss << std::put_time(&tm, "%Y-%m-%d %H:%M:%S");
  return oss.str();
}
