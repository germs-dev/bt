#pragma once

#include <span>
#include <string_view>
#include <utility>
#include <vector>

/// Summary of a trade: file, epoch, entry, exit, profit, duration
using trade_t =
    std::tuple<std::string_view, size_t, double, double, double, size_t>;

std::vector<trade_t> to_summary(std::span<const std::string_view>);
