all: btest2 assets money

assets:
	$(MAKE) -j 2 crypto stocks
	$(MAKE) convert

crypto:
	bash bin/fetch_free.sh

stocks:
	bash bin/marketstack.sh

convert:
	bash bin/convert.sh

money: btest
	/usr/bin/time build/backtest/backtest /tmp/tokens/* /tmp/stocks/*

notify:
	bash bin/notify.run

test: btest2
	/usr/bin/time build/backtest/backtest /tmp/stocks/*.csv /tmp/tokens/*.csv --benchmark_filter=

test_no_benchmark: btest2
	/usr/bin/time build/backtest/backtest /tmp/stocks/*.csv /tmp/tokens/*.csv --benchmark_filter=nofilter

clean:
	$(RM) -r /tmp/tokens /tmp/stocks build

btest:
	cmake -B build -S .
	cmake --build build

btest2:
	cmake -B build -S .
	cmake --build build --parallel

benchmark:
	ls */*.h */*.cxx CMakeLists.txt */CMakeLists.txt | entr -crs "make --silent test && build/fx/fx"

entr:
	ls */*.h */*.cxx CMakeLists.txt */CMakeLists.txt | entr -crs "make --silent test_no_benchmark"

format:
	clang-format -i */*.cxx */*.h

profile: prof.svg

prof.svg: build/backtest/backtest
	gprof $< | gprof2dot --wrap --strip | dot -Tsvg > $@

tidy:
	sort stocks.txt | uniq > /tmp/stocks.txt
	mv /tmp/stocks.txt stocks.txt
	wc -l stocks.txt

