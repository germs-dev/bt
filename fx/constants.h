#pragma once

namespace fx {

/// The number of prices in a trading window
constexpr auto win{68uz};
constexpr auto win2{34uz};
constexpr auto win4{17uz};

/// Close position if price has increased by this percentage
constexpr auto take_profit{3.0};

/// Close position if price has decreased by this percentage
constexpr auto stop_loss{take_profit * 2.0};

/// Minimum price to consider a trade
constexpr auto minimum_entry{4.3};

/// Minimum (normalised) ATR to consider a trade
constexpr auto minimum_atr{take_profit / 6.0};

/// Number of cells in a row of price data
constexpr auto cells{6uz};

/// Furthest back in time to consider a trade
constexpr auto earliest_entry_epoch{1689798790};
} // namespace fx
