#pragma once

#include "core.h"
#include <cassert>

// OHLC
namespace fx {
/// Get time of a data point
constexpr auto to_time(std::ranges::range auto &&xs) {
  return std::lround(to_first(xs));
};

/// Get open price for a data point
constexpr auto to_open(std::ranges::range auto &&xs) {
  assert(to_size(xs) > 1uz);
  return xs[1];
}

/// Get high price for a data point
constexpr auto to_high(std::ranges::range auto &&xs) {
  assert(to_size(xs) > 2uz);
  return xs[2];
}

/// Get low price for a data point
constexpr auto to_low(std::ranges::range auto &&xs) {
  assert(to_size(xs) > 3uz);
  return xs[3];
}

/// Get close price for a data point
constexpr auto to_close(std::ranges::range auto &&xs) {
  assert(to_size(xs) > 4uz);
  return xs[4];
}

/// Get total volume for a data point
constexpr auto to_volume = [](std::ranges::range auto &&xs) {
  assert(to_size(xs) > 5uz);
  return xs[5];
};
} // namespace fx