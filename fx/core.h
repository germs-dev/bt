#pragma once

#include "constants.h"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <ranges>

/// Core routines that don't depend on any other fx routines
namespace fx {

/// Calculate size of a series, return type depends on input param
template <typename T = size_t>
constexpr T to_size(std::ranges::range auto &&xs) {
  return static_cast<T>(std::ranges::size(xs));
}

/// Return the first entry in a series
constexpr auto to_first(std::ranges::range auto &&xs) {
  assert(not std::ranges::empty(xs));
  return xs[0];
}

/// Return the last entry in a series
constexpr auto to_last = [](std::ranges::range auto &&xs) {
  assert(not std::ranges::empty(xs));
  return xs[to_size(xs) - 1uz];
};

/// Calculate sum of series
constexpr auto to_sum(std::ranges::range auto &&xs) {
  return std::ranges::fold_left(
      xs, 0.0, [](const double acc, double x) { return acc + x; });
}

/// Calculate sum of series
constexpr auto to_sum2(std::ranges::range auto &&xs) {
  assert(not std::ranges::empty(xs));

  const auto &&sum = std::ranges::fold_left(
      xs, decltype(xs[0]){}, [](auto acc, auto &&x) { return acc + x; });

  // Check the type of the first element is the same as the sum
  static_assert(std::is_same_v<std::remove_cvref_t<decltype(xs[0])>,
                               std::remove_cvref_t<decltype(sum)>>);

  return sum;
}

/// Just passing through
constexpr auto identity = [](auto &&xs) { return xs; };

/// Just passing through
constexpr auto identity2 = [](auto &&xs) {
  return std::forward<decltype(xs)>(xs);
};

/// Calculate profit from a trade
constexpr auto to_profit(auto &&entry, auto &&exit) {
  auto &&profit = decltype(entry){100.0f} * (exit - entry) / entry;

  static_assert(std::is_same_v<decltype(entry), decltype(exit)>);
  static_assert(std::is_same_v<decltype(profit), decltype(entry)>);

  return profit;
}

} // namespace fx
