#include "ohlc.h"
#include <numeric>
#include <vector>

// CORE
namespace fx {
static_assert(to_size(std::views::iota(0, 10)) == 10uz);
static_assert(to_size(std::vector{1.0, 2.0}) == 2uz);
static_assert(to_size(std::vector{1, 2, 3}) == 3uz);
static_assert(to_size<double>(std::vector{1, 2, 3}) == 3.0);
static_assert(to_size<float>(std::vector<int>{}) == 0uz);

static_assert(to_first(std::vector{1, 2, 3}) == 1);
static_assert(to_first(std::vector{1.0, 2.0, 3.0}) == 1.0);
static_assert(to_first(std::vector{'a'}) == 'a');

static_assert(to_last(std::vector{1, 2, 3}) == 3);
static_assert(to_last(std::vector{1.0, 2.0, 3.0}) == 3.0);
static_assert(to_last(std::vector{'a'}) == 'a');

static_assert(to_sum(std::vector{1, 2, 3}) == 6.0);
static_assert(to_sum(std::vector{1.0f, 2.0f}) == 3.0f);
static_assert(to_sum(std::vector<int>{}) == 0.0);

static_assert(identity(1) == 1);
static_assert(std::ranges::size(identity(std::vector{2, 2})) == 2uz);

static_assert(to_profit(100.0, 100.0) == 0.0);
static_assert(to_profit(100.0f, 150.0f) == 50.0f);
static_assert(to_profit(200.0, 100.0) == -50.0);
static_assert(to_profit(200.0l, 100.0l) == -50.0l);
} // namespace fx

// DEPS
namespace fx {
// OHLC
static_assert(to_time(std::vector{1.4, 2.0, 3.0}) == 1);
static_assert(to_time(std::vector{1.6, 2.0, 3.0}) == 2);
static_assert(to_open(std::vector{0.0, 1.0, 2.0, 3.0, 4.0}) == 1.0);
static_assert(to_high(std::vector{0.0, 1.0, 2.0, 3.0, 4.0}) == 2.0);
static_assert(to_low(std::vector{0.0, 1.0, 2.0, 3.0, 4.0}) == 3.0);
static_assert(to_close(std::vector{0.0, 1.0, 2.0, 3.0, 4.0}) == 4.0);
static_assert(to_volume(std::vector{0.0, 1.0, 2.0, 3.0, 4.0, 5.0}) == 5.0);
} // namespace fx
