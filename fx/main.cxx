#include "core.h"
#include "ohlc.h"
#include "trade.h"
#include <algorithm>
#include <array>
#include <benchmark/benchmark.h>
#include <numeric>

// Create some test data for the trades
namespace {

/// An example of a full set of price data
const std::vector<std::vector<double>> xs = [] {
  auto &&xs = std::vector<std::vector<double>>{};
  auto &&row = std::vector<double>{0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f};

  for (size_t i = 0; i < 2000; ++i) {
    row[0] = static_cast<double>(i);
    xs.emplace_back(row);
  }

  return xs;
}();

/// A single row of price data
constexpr auto xs2 = std::array{1, 2, 3, 4, 5, 6};

/// A single column of price data
constexpr auto xs3 = [] {
  std::array<double, 2000> xs;
  for (auto i : std::views::iota(0, fx::to_size<int>(xs)))
    xs[i] = static_cast<double>(i);

  return xs;
}();
} // namespace

// TRADE
namespace fx {

void BM_is_entry(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(
        std::ranges::for_each(xs | slide(win) | filter(is_entry), [](auto) {}));
}

void BM_is_entry2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(std::ranges::for_each(
        xs | slide(win) | filter(is_entry2), [](auto) {}));
}

void BM_to_exit(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_exit(xs | take(win)));
}

void BM_to_exit2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_exit2(xs | take(win)));
}

void BM_to_vwap(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_vwap(xs));
}

void BM_to_vwap2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_vwap2(xs));
}

void BM_to_atr(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_atr(xs));
}

void BM_to_atr2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_atr2(xs));
}

void BM_to_atr3(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_atr3(xs));
}

void BM_is_recent_dip2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(is_recent_dip2(xs | take(win)));
}

void BM_to_average_func(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_average_func(xs3, identity));
}

void BM_to_average_func2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_average_func2(xs3, identity));
}

void BM_to_average_volume(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_average_volume(xs));
}

void BM_to_average_volume2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_average_volume2(xs));
}

BENCHMARK(BM_is_entry);
BENCHMARK(BM_is_entry2);
BENCHMARK(BM_to_exit);
BENCHMARK(BM_to_exit2);
BENCHMARK(BM_to_vwap);
BENCHMARK(BM_to_vwap2);
BENCHMARK(BM_is_recent_dip2);
BENCHMARK(BM_to_atr);
BENCHMARK(BM_to_atr2);
BENCHMARK(BM_to_atr3);
BENCHMARK(BM_to_average_func);
BENCHMARK(BM_to_average_func2);
BENCHMARK(BM_to_average_volume);
BENCHMARK(BM_to_average_volume2);

} // namespace fx

// CORE
namespace fx {
void BM_to_size(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_size(xs));
}

void BM_to_first(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_first(xs2));
}

void BM_to_last(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_last(xs2));
}

void BM_to_sum(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_sum(xs3));
}

void BM_to_sum2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_sum2(xs3));
}

void BM_to_spot(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_spot(xs2));
}

void BM_identity(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(identity(xs3));
}

void BM_identity2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(identity2(xs3));
}

void BM_to_profit(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_profit(100.0, 110.0));
}

BENCHMARK(BM_to_size);
BENCHMARK(BM_to_first);
BENCHMARK(BM_to_last);
BENCHMARK(BM_to_sum);
BENCHMARK(BM_to_sum2);
BENCHMARK(BM_to_spot);
BENCHMARK(BM_identity);
BENCHMARK(BM_identity2);
BENCHMARK(BM_to_profit);
} // namespace fx

// OHLC
namespace fx {

void BM_to_time(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_time(xs2));
}

void BM_to_open(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_open(xs2));
}

void BM_to_high(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_high(xs2));
}

void BM_to_low(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_low(xs2));
}

void BM_to_close(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_close(xs2));
}

void BM_to_volume(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(to_volume(xs2));
}

BENCHMARK(BM_to_time);
BENCHMARK(BM_to_open);
BENCHMARK(BM_to_high);
BENCHMARK(BM_to_low);
BENCHMARK(BM_to_close);
BENCHMARK(BM_to_volume);

} // namespace fx

int main(int argc, char **argv) {
  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
}