#pragma once

#include "constants.h"
#include "core.h"
#include "ohlc.h"
#include <cassert>
#include <ranges>
#include <vector>

namespace fx {

using namespace std::views;

/// Calculate average of series, applying a function to each element (which
/// could be a no-op)
constexpr auto to_average_func = [](auto &&xs, auto func) {
  assert(not std::ranges::empty(xs));
  return to_sum(xs | transform(func)) / to_size(xs);
};

constexpr auto to_average_func2 = [](auto &&xs, auto &&func) {
  assert(not std::ranges::empty(xs));

  auto &&sum =
      std::ranges::fold_left(xs | transform(func), 0.0,
                             [](const auto acc, auto x) { return acc + x; });

  return sum;
};

/// Calculate spot value, note we don't average all of the OHLC prices
constexpr auto to_spot = [](auto &&xs) {
  // Don't include the close price in the average
  assert(to_size(xs) > 4);

  auto &&spot = to_average_func(xs | drop(1) | take(3), identity);
  return spot;
};

static_assert(to_spot(std::vector{0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0}) == 2.0);

/// Calculate ATR of a series
constexpr auto to_atr(auto &&series) {
  // Get all adjacent pairs
  auto pairs = series | slide(2);

  // Calculate the true price and volume
  auto true_ranges = pairs | transform([](auto px) {
                       assert(to_size(px) == 2);
                       auto a = px[0];
                       auto b = px[1];
                       auto tr_high = std::abs(to_high(b) - to_close(a));
                       auto tr_low = std::abs(to_low(b) - to_close(a));
                       return (tr_high + tr_low) / 2.0;
                     });

  // Calculate average true range
  return to_average_func(true_ranges, identity);
}

/// Calculate ATR of a series
constexpr auto to_atr2(auto &&series) {
  // Get all adjacent pairs
  auto pairs = series | slide(2);

  // Calculate the true price and volume
  auto true_ranges = pairs | transform([](auto &&px) {
                       assert(to_size(px) == 2);
                       auto &&a = px[0];
                       auto &&b = px[1];
                       auto &&tr_high = std::abs(to_high(b) - to_close(a));
                       auto &&tr_low = std::abs(to_low(b) - to_close(a));
                       return (tr_high + tr_low) / 2.0;
                     });

  // Calculate average true range
  return to_average_func(true_ranges, identity);
}

/// Calculate ATR of a series
constexpr double to_atr3(auto &&series) {
  // Get all adjacent pairs
  auto &&pairs = series | slide(2);

  // Calculate the true price and volume
  auto &&true_ranges = pairs | transform([](auto &&px) {
                         assert(to_size(px) == 2);
                         auto &&a = px[0];
                         auto &&b = px[1];
                         auto &&tr_high = std::abs(to_high(b) - to_close(a));
                         auto &&tr_low = std::abs(to_low(b) - to_close(a));
                         return (tr_high + tr_low) / double{2.0f};
                       });

  // Calculate average true range
  return to_average_func(true_ranges, [](auto &&x) { return x; });
}

/// Calculate average volume over a series
constexpr auto to_average_volume = [](auto series) {
  return to_average_func(series, to_volume);
};

/// Calculate average volume over a series
constexpr auto to_average_volume2 = [](auto &&series) {
  return to_average_func2(series, to_volume);
};

/// Calculate VWAP rolling average
constexpr double to_vwap(std::ranges::range auto &&xs) {
  // Initialise cumulative volumes
  auto cumulative_tp_volume = 0.0;
  auto cumulative_volume = 0.0;

  // Calculate VWAP for each sliding window
  for (auto &&x : xs) {
    cumulative_tp_volume += to_spot(x) * to_volume(x);
    cumulative_volume += to_volume(x);
  }

  return cumulative_tp_volume / cumulative_volume;
}

/// Calculate VWAP rolling average
constexpr double to_vwap2(std::ranges::range auto &&xs) {
  // Initialise cumulative volumes
  auto &&cumulative_tp_volume = 0.0;
  auto &&cumulative_volume = 0.0;

  // Calculate VWAP for each sliding window
  for (auto &&x : xs) {
    auto &&vol = to_volume(x);
    cumulative_tp_volume += to_spot(x) * vol;
    cumulative_volume += vol;
  }

  return cumulative_tp_volume / cumulative_volume;
}

/// Test if there has been a recent minimum
auto is_recent_dip2(auto &&series) {
  // Find minimum spot value
  auto &&min_it = std::ranges::min_element(
      series, [](auto &&a, auto &&b) { return to_spot(a) < to_spot(b); });

  // Return true if the minimum is in the middle
  return std::ranges::distance(std::ranges::begin(series), min_it) ==
         to_size<ssize_t>(series) / 2;
}

/// Find an exit in a series
auto to_exit = [](auto &&series) {
  // Calculate the exit thresholds
  const double open_price = to_spot(to_first(series));
  const double take_price = open_price * (100.0 + take_profit) / 100.0;
  const double stop_price = open_price * (100.0 - stop_loss) / 100.0;

  // Drop everything up to the exit
  auto to_trunc =
      series | drop_while([=](auto &&xs) {
        const double close_price = to_spot(xs);
        return close_price > stop_price and close_price < take_price;
      }) |
      common;

  auto truncated = std::vector(std::begin(to_trunc), std::end(to_trunc));

  // If it's been truncated to nothing, then return the last price
  if (std::ranges::empty(truncated))
    return to_last(series);

  // Otherwise, the first price in the truncated series is the exit
  return to_first(truncated);
};

/// Find an exit in a series
auto to_exit2 = [](auto &&series) {
  // Calculate the exit thresholds
  auto &&open_price = to_spot(to_first(series));
  auto &&take_price = open_price * (100.0 + take_profit) / 100.0;
  auto &&stop_price = open_price * (100.0 - stop_loss) / 100.0;

  // Drop everything up to the exit
  auto &&to_trunc =
      series | drop_while([&](auto &&xs) {
        auto &&close_price = to_spot(xs);
        return close_price > stop_price and close_price < take_price;
      }) |
      common;

  auto &&truncated = std::vector(std::begin(to_trunc), std::end(to_trunc));

  // If it's been truncated to nothing, then return the last price
  // Otherwise, the first price in the truncated series is the exit
  return std::ranges::empty(truncated) ? to_last(series) : to_first(truncated);
};

/// Calculate if the final price is an entry
auto is_entry = [](std::ranges::range auto &&series) {
  // Create subsets of complete series
  const auto last_half =
      series | drop(std::llround(to_size<double>(series) / 2.0));
  const auto last_quarter =
      series | drop(std::llround(to_size<double>(series) * 3.0 / 4.0));

  // Copy the final price: the entry point
  const auto entry = to_last(series);

  // Test all the criteria for a trade
  return
      // No small change
      to_spot(entry) > fx::minimum_entry

      // Above minimum ATR
      and (100.0 * to_atr3(series) / to_spot(entry)) > fx::minimum_atr

      // Average volume is used to determine if the last price is a spike
      and
      to_volume(entry) > to_average_volume(series | take(to_size(series) - 1))

      // There has been a recent minimum
      and is_recent_dip2(last_quarter)

      // Price is below the long VWAP
      and to_spot(entry) < to_vwap(series)

      // Short VWAP is above the long VWAP
      and to_vwap(last_half) > to_vwap(series);
};

/// Calculate if the final price is an entry
auto is_entry2 = [](std::ranges::range auto &&series) {
  assert(not std::ranges::empty(series));

  // Create subsets of complete series
  // Using reverse simplifies the drop logic
  auto &&last_half = series | reverse | take(fx::win2) | reverse;
  auto &&last_quarter = series | reverse | take(fx::win4) | reverse;

  // Copy the final price: the entry point
  auto &&entry = to_last(series);

  // Test all the criteria for a trade
  return

      // No old trades
      to_time(entry) > fx::earliest_entry_epoch

      // No small change
      and to_spot(entry) > fx::minimum_entry

      // Above minimum ATR
      and (100.0 * to_atr3(series) / to_spot(entry)) > fx::minimum_atr

      // Average volume is used to determine if the last price is a spike
      and to_volume(entry) >
              to_average_volume(series | reverse | drop(1uz) | reverse)

      // There has been a recent minimum
      and is_recent_dip2(last_quarter)

      // Price is below the long VWAP
      and to_spot(entry) < to_vwap(series)

      // Short VWAP is above the long VWAP
      and to_vwap(last_half) > to_vwap(series);
};

} // namespace fx